#!/usr/bin/env Python
import os
import re
import chardet
import shutil
import subprocess
import usdxsong


# Input : List of file locations, Context(Directory in which it runs, relative to current working directory)
def convenc(files, context='.'):
	# Iterate through filelist
	for file in files:
		# File with context
		cfile = os.path.join(context, file)
		# Check if it is a txt-file
		if file.lower().endswith(".txt"):
			# Open as bytestream
			raw = open(cfile, "rb").read()
			# Extract lyrics
			lyrics = extractLyrics(raw)
			# Detect encoding
			encoding = chardet.detect(lyrics)['encoding']
			# Check if encoding is different from regular detection
			if encoding != chardet.detect(raw)['encoding']:
				print("Enc Changed : " + cfile)
			tmpfile = cfile + ".tmp"
			# Change encoding
			changeEncoding(cfile, tmpfile, encoding)


# Recursive Converter
def reconvenc(directory):
	# Find files
	dirs, files = scan(directory)
	# Convert all files
	convenc(files, directory)

	# repeat for all subfolders
	for subdir in dirs:
		reconvenc(subdir)


def extractLyrics(bytestream):
	text = ""
	# Split string into lines
	lines = bytestream.splitlines()
	for line in lines:
		# Check if it is a line with text
		r = re.match("[:*f] \d+ \d+ \-?\d+ ( ?[^\s\-]+ ??)(?: ?- ?)?$", line)
		if r:
			# Add text to textvariable
			text += r.group(1)
		# Check if it is a break line
		elif re.match("- ?\d+", line):
			# Add a space to textvariable
			text += " "
	return text


# Scan for strange .txt behavior
def txtScanner(directory):
	# scan for subdirectories
	dirs = scan(directory)[0]
	for dir in dirs:
		# scan for files
		files = scan(dir)[1]
		txtNumber = 0
		emptyNumber = 0
		# For each file
		for f in files:
			# Check if it is a textfile
			if (f.lower().endswith(".txt")):
				# and increase textfilescounter
				txtNumber = txtNumber + 1
			# Check if it is empty
			if (os.stat(f).st_size == 0):
				# and increase emptyfilescounter
				emptyNumber = emptyNumber + 1
		if (emptyNumber > 0):
			print(dir + " has " + str(emptyNumber) + " empty files")
		if (txtNumber != 1):
			print(dir + " has " + str(txtNumber) + " .txt files")


# Scan for strange .mp3 behavior
def mp3Scanner(directory):
	dirs = scan(directory)[0]
	for dir in dirs:
		files = scan(dir)[1]
		mp3Number = 0
		emptyNumber = 0
		oggfileNumber = 0
		for f in files:
			if f.lower().endswith(".mp3"):
				mp3Number = mp3Number + 1
			if f.lower().endswith(".ogg"):
				oggfileNumber = oggfileNumber + 1
			if os.stat(f).st_size == 0:
				emptyNumber = emptyNumber + 1
		if emptyNumber > 0:
			print(dir + " has " + str(emptyNumber) + " empty files")
		print(dir + " has " + str(mp3Number) + " .mp3 and " + str(oggfileNumber) + " .ogg files")


# Convert ogg to mp3
def convoggtomp3(directory, overwritemp3=False):
	# Find Files
	dirs, files = scan(directory)
	mp3 = []
	ogg = []
	txt = []
	# Find ogg files
	for file in files:
		if file.lower().endswith(".ogg"):
			ogg.append(file)
		if file.lower().endswith(".txt"):
			txt.append(file)
		if file.lower().endswith(".mp3"):
			mp3.append(file)

	if len(txt) == 0:
		print(directory + ' has no Textfile')

	elif (overwritemp3 or (mp3) == 0) and len(ogg) > 0:
		for file in ogg:
			print(file)
			# Replace Whitespaces with Backslash+whitespace
			file.replace(' ', '\ ')
			# New name
			newFile = file.rsplit(".", 1)[0] + ".mp3"
			# Convert
			ffmpeg = subprocess.Popen(["ffmpeg", "-i", file, "-acodec", "libmp3lame", newFile])
			ffmpeg.wait()
			for f in txt:
				song = usdxsong.usdxsong(f)
				success, str = song.relinkfiles(True)
				if not success:
					print(str + ' Error wile relinking')
					print(file)
				if success:
					success, str = song.writeToFile()
				if not success:
					print(str + ' Error wile writing')
					print(file)

	# Repeat for Subdirectories
	for subdirectory in dirs:
		convoggtomp3(subdirectory, overwritemp3)


def findAllTxt(directory):
	txt = []
	dirs, files = scan(directory)
	for file in files:
		if file.lower().endswith(".txt"):
			txt.append(file)
	for dir in dirs:
		txt.extend(findAllTxt(dir))
	return txt


def correctall(directory):
	# Find all .txt within the directory all subdirectories
	txt = findAllTxt(directory)

	# Repair all of them:
	# Open Errorfile where all Errors are written down
	with open(os.path.join(directory, 'Errors.txd'), 'w', encoding="UTF-8") as errors:
		with open(os.path.join(directory, 'Corrupted Songs.txd'), 'w', encoding="UTF-8") as corrupt:
			for file in txt:
				# Print Songname as Form of "Progressbar"
				print(os.path.basename(file))
				# Try Loading, else write to error file
				try:
					song = usdxsong.usdxsong(file)
				except Exception as ERROR:
					corrupt.write("EXCEPTION: \n")
					corrupt.write(file + "\n")
					corrupt.write(str(type(ERROR)) + "\t" + str(ERROR) + "\n")
					continue
				# Relink, if errors write to error file
				success, msg = song.relinkfiles(True)
				if not success and msg == "MP3":
					corrupt.write("RELINKERROR: \n")
					corrupt.write(file + "\n")
					corrupt.write("Wrong number of mp3s.\n")
				elif not success and msg == 'VIDEO-NUMBER':
					corrupt.write("RELINKERROR: \n")
					corrupt.write(file + "\n")
					corrupt.write("Wrong number of videos.\n")
				else:
					# Remove freestyle
					song.removeFreestyle()
					# Get Errors
					(E1, E2, E3, E4) = song.echoerrors(False, False, False, False)
					if E4 or (E2 and E3):  # Bad Errors result in unwritten file
						corrupt.write("CORRUPTED FILE: \n")
						corrupt.write(file + "\n")
						corrupt.write('Repaired: ' + str(E1) + ", Problems: " + str(E2) + ", Other: " + str(
							E3) + ", Vitals: " + str(E4) + "\n")
						corrupt.write("Song not written!\n")
					elif E3 or E2:  # Light errors result in written file(not as bad because of Backup)
						success, INF = song.writeToFile('', True, True, )
						if success:
							errors.write("NOT REPAIRED ERRORS: \n")
							errors.write(file + "\n")
							errors.write('Repaired: ' + str(E1) + ", Problems: " + str(E2) + ", Other: " + str(
								E3) + ", Vitals: " + str(E4) + "\r\n")
							errors.write("Song not written!\n")
						else:
							corrupt.write("COULD NOT REPAIR: \n")
							corrupt.write(file + "\n")
							corrupt.write('Repaired: ' + str(E1) + ", Problems: " + str(E2) + ", Other: " + str(
								E3) + ", Vitals: " + str(E4) + "\n")
							corrupt.write(INF + "\n")
							corrupt.write("Song not written!\n")
					else:  # No Errors result in written file
						success, INF = song.writeToFile('', True, True, )
						if not success:
							corrupt.write("COULD NOT WRITE: \n")
							corrupt.write(file + "\n")
							corrupt.write(INF + "\n")
							corrupt.write("Song not written!\n")


# TODO ??? WORKLIST is array of absolute paths to dirs, DIRLIST is array of absolute paths to files
def findbadchars(DIR):
	(WORKLIST, DIRLIST) = scan(DIR)
	TXT = dict()
	for D in DIRLIST:
		(WL, DL) = scan(D)
		for W in WL:
			if (W.endswith(".txt") or W.endswith(".TXT")):
				try:
					song = usdxsong.usdxsong(W)
					wString = song.tags["TITLE"] + song.tags["ARTIST"]
					for time in song.p0song[0]:
						wString = wString + song.p0song[0][time][3]
					for time in song.p1song[0]:
						wString = wString + song.p1song[0][time][3]
					for time in song.p2song[0]:
						wString = wString + song.p2song[0][time][3]
					for time in song.p3song[0]:
						wString = wString + song.p3song[0][time][3]
					regx = re.compile("[^\s\\!@#$€%&^~()/=+*,:;`´.?<\'>§[°\]’\-…a-zöäüßA-ZÖÄÜÀ-ÖØ-Üà-öø-üĀ-ž\d\"]")
					badchars = re.findall(regx, wString)
					for C in badchars:
						if C in TXT:
							TXT[C].append(W)
						else:
							TXT[C] = [W]
				except Exception as ERROR:
					print(W)
	return (TXT)


# TODO ??? WORKLIST is array of absolute paths to dirs, DIRLIST is array of absolute paths to files
def replacebadchars(DIR):
	(WORKLIST, DIRLIST) = scan(DIR)
	for D in DIRLIST:
		(WL, DL) = scan(D)
		for W in WL:
			if (W.endswith(".txt") or W.endswith(".TXT")):
				try:
					SONG = usdxsong.usdxsong(W)
					(errorR, errorP, errorO, errorV) = SONG.echoerrors(False, False, False, False)
					if not (errorP or errorO or errorV):
						SONG.tags["TITLE"] = _repbadchars(SONG.tags["TITLE"])
						SONG.tags["ARTIST"] = _repbadchars(SONG.tags["ARTIST"])
						for time in SONG.p0song[0]:
							SONG.p0song[0][time][3] = _repbadchars(SONG.p0song[0][time][3])
						for time in SONG.p1song[0]:
							SONG.p1song[0][time][3] = _repbadchars(SONG.p1song[0][time][3])
						for time in SONG.p2song[0]:
							SONG.p2song[0][time][3] = _repbadchars(SONG.p2song[0][time][3])
						SONG.writeToFile('', True, True, )
					else:
						print(W + ' has Problems')
				except Exception as ERROR:
					print(W + " throws Errors")


def _repbadchars(STRING):
	STRING = STRING.replace("", "`")
	STRING = STRING.replace("", "œ")
	STRING = STRING.replace("큦", "'")
	STRING = STRING.replace("큞", "'")
	STRING = STRING.replace("я", "ß")
	STRING = STRING.replace("ь", "ü")
	STRING = STRING.replace("δ", "ä")
	STRING = STRING.replace("φ", "ö")
	STRING = STRING.replace("ί", "ß")
	STRING = STRING.replace("ת", "u")
	STRING = STRING.replace("ב", "a")
	STRING = STRING.replace("ת", "u")
	STRING = STRING.replace("ף", "o")
	STRING = STRING.replace("סo", "n")
	STRING = STRING.replace("ם", "i")
	STRING = STRING.replace("י", "e")
	STRING = STRING.replace("סי''", "ne")
	STRING = STRING.replace("Ăź", "ü")
	STRING = STRING.replace("Ă¤", "ä")
	STRING = STRING.replace("째", "°")
	STRING = STRING.replace("รง", "ç")
	STRING = STRING.replace("รฉ", "é")
	STRING = STRING.replace("ц", "ö")
	STRING = STRING.replace("д", "ä")
	STRING = STRING.replace("ƒ", "f")
	STRING = STRING.replace("ĂÂ´", "'")
	STRING = STRING.replace("", "'")
	STRING = STRING.replace("ό", "ä")
	STRING = STRING.replace("δ", "ä")
	STRING = STRING.replace("Δ", "ä")
	STRING = STRING.replace("ĂÂ", "...")
	STRING = STRING.replace("", '"')
	STRING = STRING.replace("", '"')
	STRING = STRING.replace("知", "'m")
	STRING = STRING.replace("й", "é")
	STRING = STRING.replace("ά", "Ü")
	STRING = STRING.replace("Ă", "Ö")
	STRING = STRING.replace("ŕ¸Łŕ¸", "ö")
	STRING = STRING.replace("ŕ¸Łŕ¸", "o")
	STRING = STRING.replace("№", "Ü")
	STRING = STRING.replace("端", "ü")
	STRING = STRING.replace("Ă", "ß")
	STRING = STRING.replace("â€™", "'")
	STRING = STRING.replace("", " ")
	STRING = STRING.replace("큞", "'")
	STRING = STRING.replace("채", "ä")
	STRING = STRING.replace("", '"')
	STRING = STRING.replace("", "")
	STRING = STRING.replace("ÄĹş", "ü")
	STRING = STRING.replace("С", "`")
	STRING = STRING.replace("채", "ä")
	STRING = STRING.replace("체", "ü")
	STRING = STRING.replace("Φ", "Ö")
	STRING = STRING.replace("窶話", "'b")
	STRING = STRING.replace("â", "'")
	STRING = STRING.replace("Т", "' ")
	STRING = STRING.replace("知", "'m")
	STRING = STRING.replace("痴", "'s")
	STRING = STRING.replace("池", "'r")
	STRING = STRING.replace("値", "'")
	return (STRING)


def restorebkup(DIR, OLDEST=False):
	(DIRLIST, WORKLIST) = scan(DIR)
	for STUFF in WORKLIST:
		# Ist noch scheisse
		if STUFF.endswith(".bkup"):
			if OLDEST and not STUFF.endswith("(newest).bkup"):
				os.replace(STUFF, STUFF.rsplit(".", 1)[0] + ".txt")
			# weil wenn er neustes Bakup laden soll, sollte er auch das aeltere nehmen wenn es keine neuen gibt
			elif not OLDEST and STUFF.endswith("(newest).bkup"):
				os.replace(STUFF, STUFF.rsplit("(newest).", 1)[0] + ".txt")
	for DIRS in DIRLIST:
		restorebkup(DIRS, OLDEST)


def getalltxt(DIR, DESTINY):
	if not os.path.exists(DESTINY):
		return (DESTINY + " is no Directory")
	# Else lets go
	(DIRLIST, WORKLIST) = scan(DIR)
	for STUFF in WORKLIST:
		if STUFF.endswith(".txt"):
			shutil.copy2(STUFF, DESTINY)
	for DIRS in DIRLIST:
		getalltxt(DIRS, DESTINY)


def scan(dir):
	try:
		# Convert path to absolute path and convert os path seperator (/ on linux, \ on windows)
		return [[os.path.abspath(os.path.join(dir, y)) for y in x] for x in next(os.walk(dir))[1:]]
	except StopIteration:
		return ([], [])


# Replace File
def changeEncoding(FILE, NEWFILE, ENCODING):
	try:
		with open(FILE, 'r', encoding=ENCODING) as SOURCE:
			with open(NEWFILE, 'w', encoding="UTF-8") as RESULT:
				for line in SOURCE:
					RESULT.write((line[:-1] + "\r\n"))
		os.replace(NEWFILE, FILE)
	except Exception as ERROR:
		print("File throws strange Errors")
		print(FILE)
