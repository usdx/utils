#!/usr/bin/env Python
import os
import re
import time
# import subprocess
import usdxutils


# ------------------------- #
# Song Object for Handling #
# ------------------------- #
class usdxsong(object):
	# The Constructor to read in the .txt into Songobjects
	# Be Careful, the Object only keeps the basic song structure. e.g. the above TAGs, followed by lyrics, followed by an E
	# strange, but common, .txt structures will be converted into the basic format
	def __init__(self, PATH):

		# ------------------------------------ #
		# ----- DEFINE INSTANT VARIABLES ----- #
		# ------------------------------------ #

		# The Songs own Path:
		self.path = PATH

		# Is the Song a Duett?
		self.duett = True

		# Was anything Changed?
		# Dosnt work yet
		self.changed = False

		# The Song TAGs
		# Be Careful, Other TAGs will be lost! (But other TAGs are also unnecessary in like every possible Case, therfore: Who cares?)
		self.tags =  dict([('TITLE', ''), ('ARTIST', ''), ('ALBUM', ''), ('MP3', ''), ('VIDEO', ''), ('COVER', ''), ('BACKGROUND', ''),
						   ('P1', ''), ('P2', ''), ('BPM', ''), ('GAP', ''), ('VIDEOGAP', ''), ('MEDLEYSTARTBEAT', ''), ('MEDLEYENDBEAT', ''),
						   ('ENCODING', 'UTF8'), ('PREVIEWSTART', ''), ('LANGUAGE', ''), ('GENRE', ''), ('EDITION', ''),  ('YEAR', ''),
						   ('CREATOR', ''), ('AUTHOR', ''), ('START', ''), ('END', ''), ('RELATIVE', '')])
		# Common Misspellings of Tags
		self.tag_aliases = {
			'AUTOR'        : 'AUTHOR',
			'DUETSINGERP1' : 'P1',
			'DUETSINGERP2' : 'P2',
			'EDTION'       : 'EDITION',
			'GAPVIDEO'     : 'VIDEOGAP',
			'GENRER'       : 'GENRE',
			'LANUGAE'      : 'LANGUAGE',
			'LANGAUGE'     : 'LANGUAGE',
			'NOTESGAP'     : 'GAP',
			'YEARS'        : 'YEAR'
		}

		#Unknown but found tags:
		# RESOLUTION (always in steps of 8)
		# KEY (found beeing 0, 2 or 3)

		# Players lyrics. P0 is standard, P1 and P2 are the singers in a Duett, P3 is the Part of the Duett which both sing.
		# SYNTAX is: [Notes Dictionary, Breaks Dictionary]
		# In both Dictionaries the Key is the Time an Event occurs.
		# If there are 2 Notes or 2 Breaks which occur simultaniously an DoubleTimeNote(DTN) will be added to self.vitals
		# This should NEVER occur as you can not sing 2 notes simultaniously and 2 Breaks simultaniusly are nonsens
		# A Note and a breack can and will occur simultaniusly, when there is no time to wait for a extra beat to break the line.
		# SYNTAX: PLAYER_DICTONARY: [ TIME: [TYPE,LENGTH,PITCH,LYRIC] , TIME: [TYPE,BREAK_END_(optional)]]
		self.p0song = [dict(), dict()]
		self.p1song = [dict(), dict()]
		self.p2song = [dict(), dict()]
		self.p3song = [dict(), dict()]

		# Error dictionaries:
		# -----------------
		# Vitals are Errors that need Correction before writing!!!
		# DTN = DoubleTimeNote = A Second Note/GoldenNote/FreestyleNote/LineBreak at the exact same moment as an already existing Note.
		# DTNs don't lead Ultrastar to Crash, but should NEVER OCCUR!!! as they make songs unsingable
		# Please correct any DTNs by Hand, self.write() will only write the first appearrence of any DTN into a File.
		# DTN SYNTAX is equal to SYNTAX in "self.problems"
		self.vitals = dict([('DTN', [False, []]), ('DuettPlayerError', False)])

		# repaired are errors that are (or will be at write) already repaired and corrected
		# These Errors are mostley minor problems which are "repaired" by beeing ignored!!!
		# MissEnd 		: The E at the End is missing. Will be corrected
		# WrongEnds 	: Somewhere not at the End is an single E, Will not be written
		# UnusualLine 	: Somewhere is an strange Line which dosnot meat the expectations, syntax is the same as in "self.problems", Will not be written
		# UnknownTag		: Somewhere is an Strange unknown Tag, will not be written
		# StrangeLyric	: Somewhere is a Strange Lyric that contains 2 "Words" on one Note. This is bad style, but will be written as it is "correct" Syntax, just unsingable. SYNTAX same as in Problems.
		self.repaired = dict(
				[('MissEnd', False), ('WrongEnds', False), ('UnknownTag', [False, []]), ('StrangeLyric', [False, []])])

		# problems are Errors that can be corrected manually, or can also be ignored, in which case this problematic lines will be left out at write()
		# This may result in missing lines at singing!
		# SYNTAX:
		# string: [Boolean,[strings]_list]
		# "Kind of errornous Note", ["Is there an Error?",["Player who should sing this Line"+"Line that throws Errors"]]
		self.problems = dict([('UnusualLine', [False, []]), ('StrangeLinebreaks', [False, []])])

		# others are errors that will not result in an unreadable(for Ultrastar) song, but could result in an unsangeble mess
		# Mostly Timing issues as Simultanious Notes to sing. Here are every Beat that needs you to sing simultaniously.
		self.others = dict([('SimNoteP0', [False, []]), ('SimNoteP1', [False, []]), ('SimNoteP2', [False, []]),
							('SimNoteP3', [False, []])])

		# ------------------------------------- #
		# ----- END OF INSTANCE VARIABLES ----- #
		# ------------------------------------- #

		# set Player to standard P0 e.g. No Duet
		PLAYER = 0

		# Read in the Song
		try:  # Main TRY Block
			# Read in the regular expected Information
			with open(self.path, 'r', encoding="UTF-8") as SOURCE:
				# Check in the Plain, if txt ends correctly, e.g. with an single E
				PLAIN = SOURCE.read()
				if not re.search("[-:F*] \d+(?: \d+ -?\d+ .+)?\s*\nE\s*$", PLAIN):
					self.repaired['MissEnd'] = True
				# If the Song doesn't end properly, it will be corrected
				if re.search("[-:*] \d+(?: \d+ -?\d+ .+)?\s*\nE\S+", PLAIN):
					self.repaired['WrongEnds'] = True

				self.checkRelative(PLAIN)

				# absolute time variable for use in case of relative files
				abstime = 0
				if self.tags['RELATIVE'].upper() == 'YES':
					print(os.path.basename(self.path).strip(".txt") + ': this Song is Relative')
				# Go trough the SOURCE
				for line in PLAIN.splitlines(True):
					# Check for strange Lines Lines, that would fuck up everything
					# Lines are allowed to start with:
					# # for TAGs
					# : for regular Lyrics
					# - for Linebreaks
					# * for Golden Notes
					# F for freestyle Lyrics
					# E for the End
					# P1, P2, P3 as the "Players" in a Duett
					if not re.match("#|:|\*|F|-|P ?0|P ?1|P ?2|P ?3|E", line):
						self.addproblem('UnusualLine', line, PLAYER)
						line = ' '

					# Remove linebreaks
					line = line.strip("\n\r")

					# Read in known TAGs
					if line.startswith('#'):
						tagLine = re.match("#(\S+):(.+)", line)
						if tagLine and tagLine.group(1).upper() in self.tags.keys():
							self.tags[tagLine.group(1).upper()] = tagLine.group(2)
						elif tagLine and tagLine.group(1).upper() in self.tag_aliases.keys():
							self.tags[self.tag_aliases[tagLine.group(1).upper()]] = tagLine.group(2)
						else:
							self.repaired['UnknownTag'][0] = True
							self.repaired['UnknownTag'][1].append(line)

					# Read in Lyrics:
					# If Lyrics are not Relative then expect standard Notation:
					elif self.tags['RELATIVE'].upper() != 'YES':
						# If line is a player change(in a duett), change Player
						if line[:2] == 'P0' or line[:3] == 'P 0':
							PLAYER = 0
						elif line[:2] == 'P1' or line[:3] == 'P 1':
							PLAYER = 1
						elif line[:2] == 'P2' or line[:3] == 'P 2':
							PLAYER = 2
						elif line[:2] == 'P3' or line[:3] == 'P 3':
							PLAYER = 3

						# If line is a regular Lyric, golden note or freestyle lyric, write it to the Player who sings it
						if line[0] == ':' or line[0] == '*' or line[0] == 'F':
							# Split the Line into 5 Parts
							(SUC, INFO) = self.linesplitter(line, PLAYER)
							if SUC:
								# If Linesplitting was successfull write it.
								# If not, Linesplitter will have already written Error
								self.writenote(PLAYER, INFO[0], INFO[1], INFO[2], INFO[3], INFO[4])


						# If line is a linebreak, write it to the Player who 'sings' it
						elif line[0] == '-':
							INFO = re.match("- ?(\d+) ?(\d+)?", line)
							# If there are not 1 or 2 entrys in an linebreak, write Problematic Lines into problems
							if not INFO:
								self.addproblem('StrangeLinebreaks', line, PLAYER)
							else:

								# Check if the both entries are integers
								# Reformat numbers to Ints to work with them effectivley
								try:
									# If no exceptions are raised
									# Write linebreak to the Singing Player
									# Use startbeat as DictonaryKEY
									self.writebreak(PLAYER, int(INFO.group(1)))
								except ValueError as ERROR:
									# Write Problematic Lines into problems
									self.addproblem('StrangeLinebreaks', line, PLAYER)

					# Else Lyrics are relative, therefore expect relative Notation:
					# Relative Notation will be automatically converted into standard notation, because fuck Relative Notation
					# This is done by Help of the absolute Time Variable: abstime
					# abstime counts up from 0 at every linebreak so it always compensates for the relative-absolute time difrence
					# abstime will be added to relative time to get the real beat at which events occur.
					else:
						# If line is a player change(in a duet), change Player
						if line[:2] == 'P0' or line[:3] == 'P 0':
							PLAYER = 0
						elif line[:2] == 'P1' or line[:3] == 'P 1':
							PLAYER = 1
						elif line[:2] == 'P2' or line[:3] == 'P 2':
							PLAYER = 2
						elif line[:2] == 'P3' or line[:3] == 'P 3':
							PLAYER = 3

						# If line is a regular Lyric, golden note or freestyle lyric, write it to the Player who sings it
						if line[0] == ':' or line[0] == '*' or line[0] == 'F':
							# Split the Line into 5 Parts
							(SUC, INFO) = self.linesplitter(line, PLAYER)
							if SUC:
								# If Linesplitting was successfull write it.
								# If not, Linesplitter will have already written Error
								self.writenote(PLAYER, INFO[0], INFO[1] + abstime, INFO[2], INFO[3], INFO[4])

						# If line is a linebreak, write it to the Player who 'sings' it
						if line[0] == '-':
							INFO = re.match("- ?(\d+) (\d+)", line)
							# If there are not 2 entries in an linebreak write Problematic Lines into problems
							if not INFO:
								self.addproblem('StrangeLinebreaks', line, PLAYER)
							else:
								INFO = [INFO.group(1),
										INFO.group(2)]
								# Check if both entries are integers
								# Reformat numbers to Ints to work with them effectivley
								try:
									INFO[0] = int(INFO[0]) + abstime
									INFO[1] = int(INFO[1])
									abstime += int(INFO[1])  # Add Length of last Line to absolute Time
									# If no exceptions are raised
									# Write linebreak to the Singing Player
									# Use startbeat as DictonaryKEY
									self.writebreak(PLAYER, INFO[0])
								except ValueError as ERROR:
									# Write Problematic Lines into problems
									self.addproblem('StrangeLinebreaks', line, PLAYER)

				# After Reading in a Relative File, this File is not Relative any more, and also will not be printed relative
				# Therefor the Relative Tag is Unneccacery and is going to be removed
				self.tags['RELATIVE'] = ''
		# Finished reading
		# Errorhandling
		except Exception as ERROR:
			print(os.path.basename(self.path) + ": File throws strange Errors!, It wasn't read in!")
			raise ERROR

		# Set Duet Variable correct:
		self.duettscan()

		# If song is a duet, split player 3
		if self.duett:
			self.splitP3()

		# Scan for Timingerrors:
		self.timingscan()

		# Remove Emty Lines from Problems:
		self.removeemptylines()

	# ------------------------- #
	# --- End of __init__() --- #
	# ------------------------- #
	# Start of Object Funktions #
	# ------------------------- #

	# Searches for .mp3, .jpg, and Videofiles in same folder, and if Names dont match TAGs, it corrects the TAGs
	# Returns: (Did Something[True/False],'Is there a problem [empty string if no, else some desciption]')
	def relinkfiles(self, mp3only=False):
		# Initialize Shit
		(PATH, SONG) = os.path.split(self.path)
		(DIRS, FILES) = usdxutils.scan(PATH)
		MUSIC = []
		PICS = []
		VIDEO = []
		DO = False
		VIDEONAMES = { 'avi', 'divx', 'flv', 'm1v', 'mkv', 'mov', 'mp4', 'mpeg', 'mpg', 'ts', 'vob', 'wmv', }
		PICNAMES = { 'bmp', 'gif', 'jpeg', 'jpg', 'png', }
		if mp3only:
			MUSICNAMES = { 'mp3' }
		else:
			MUSICNAMES = { 'm4a', 'mp3', 'mp4', 'ogg', }

		# Find Files in Directory
		for F in FILES:
			if F.rsplit('.', 1)[1].lower() in MUSICNAMES:
				MUSIC.append(os.path.basename(F))
			if F.rsplit('.', 1)[1].lower() in VIDEONAMES:
				VIDEO.append(os.path.basename(F))
			if F.rsplit('.', 1)[1].lower() in PICNAMES:
				PICS.append(os.path.basename(F))

		# Remove mp4s from music, if dedicated musicfile exists
		if len(MUSIC) > 1 and any(x in VIDEO for x in MUSIC):
			MUSIC = list( set( MUSIC ) - set( VIDEO ) )

		# Check if Music is correctly linked, only 1 Music file per Dictionary
		if len(MUSIC) == 1:
			if MUSIC[0] != self.tags['MP3']:
				self.tags['MP3'] = MUSIC[0]
				DO = True
		else:
			return (False, 'MP3')

		# Check if Viedeo is correctly Linked:
		if len(VIDEO) == 1:
			if VIDEO[0] != self.tags['VIDEO']:
				self.tags['VIDEO'] = VIDEO[0]
				DO = True
		else:
			if len(VIDEO) > 1:
				return (False, 'VIDEO-NUMBER')
			if len(VIDEO) == 0 and self.tags['VIDEO'] != '':
				self.tags['VIDEO'] = ''
				DO = True

		# Check if Pics are correctly linked
		if len(PICS) > 0:
			if self.tags['COVER'] not in PICS:
				self.tags['COVER'] = PICS[-1]
				DO = True
			if (self.tags['BACKGROUND'] != '') and (self.tags['BACKGROUND'] not in PICS):
				self.tags['BACKGROUND'] = PICS[0]
				DO = True
		else:
			self.tags['COVER'] = ''
			self.tags['BACKGROUND'] = ''
			DO = True
		# Return Success
		if DO:
			return (DO, 'Succsessfully relinked')
		else:
			return (DO, '')

	# Changes Pitch of LineNumber
	def changepitch(self, PLAYER, LINENUMBER, NEWPITCH):
		# Choose to correct Player
		if type(PLAYER) is int and 0 <= PLAYER <= 3:
			playerdict = eval("self.p" + str(PLAYER) + "song")
		else:
			raise ValueError("Player needs to be an integer between 0 and 3")
		try:
			playerdict[0][LINENUMBER][2] = NEWPITCH
		except KeyError as Error:
			return (False)
		return (True)

	# Changes Length of LineNumber
	# Removes Lines by Legth<=0
	def changelinelength(self, PLAYER, LINENUMBER, NEWLENGTH):
		# Choose to correct Player
		if type(PLAYER) is int and 0 <= PLAYER <= 3:
			playerdict = eval("self.p" + str(PLAYER) + "song")
		else:
			raise ValueError("Player needs to be an integer between 0 and 3")
		try:
			if NEWLENGTH > 0:
				playerdict[0][LINENUMBER][1] = NEWLENGTH
			else:
				playerdict[0].pop(LINENUMBER)
		except KeyError as Error:
			return (False)
		return (True)

	# Changes Length of BREAK
	# If Length = 0 => Singlelinebreak, else Doublelinebreak
	# Removes Lines by Legth<0
	def changebreaklength(self, PLAYER, LINENUMBER, NEWLENGTH):
		# Choose to correct Player
		if type(PLAYER) is int and 0 <= PLAYER <= 3:
			playerdict = eval("self.p" + str(PLAYER) + "song")
		else:
			raise ValueError("Player needs to be an integer between 0 and 3")
		try:
			if NEWLENGTH > 0:
				playerdict[1][LINENUMBER][1] = NEWLENGTH
			elif NEWLENGTH < 0:
				playerdict[1].pop(LINENUMBER)
			else:
				playerdict[1][LINENUMBER] = ['-']
		except KeyError as Error:
			return (False)
		return (True)

	# Changes Linetype for PLAYER between beat START to FINISH from BAD to GOOD
	def changelinetypes(self, START, FINISH, BAD='F', GOOD=':', PLAYER=0):
		# Choose to correct Player
		if type(PLAYER) is int and 0 <= PLAYER <= 3:
			playerdict = eval("self.p" + str(PLAYER) + "song")
		else:
			raise ValueError("Player needs to be an integer between 0 and 3")
		for key in [x for x in playerdict[0].keys() if START <= x <= FINISH]:
			# Try to change all lines between start and finish
			if playerdict[0][key][0] == BAD:
				playerdict[0][key][0] = GOOD

	# Basicly Changelinetype with correct inputs so you dont have to think for yourself
	def removeFreestyle(self, SMART=True):
		self.duettscan()
		if not (self.duett):  # If it is not a Duett
			if SMART:
				(SUC, LISTE) = self._checkVerseforFreestyle(0)
				for VERSE in LISTE:
					self.changelinetypes(VERSE[0], VERSE[1], 'F', ':', 0)
			else:
				lastbeat = max(self.p0song[0].keys())
				self.changelinetypes(0, lastbeat, 'F', ':', 0)
		else:
			if SMART:
				(SUC1, LISTE1) = self._checkVerseforFreestyle(1)
				(SUC2, LISTE2) = self._checkVerseforFreestyle(2)
				for VERSE in LISTE1:
					self.changelinetypes(VERSE[0], VERSE[1], 'F', ':', 1)
				for VERSE in LISTE2:
					self.changelinetypes(VERSE[0], VERSE[1], 'F', ':', 2)
			else:
				lastbeat1 = max(self.p1song[1].keys())
				lastbeat2 = max(self.p2song[1].keys())
				self.changelinetypes(0, lastbeat1, 'F', ':', 1)
				self.changelinetypes(0, lastbeat2, 'F', ':', 1)

	# Check Verses for Freestyle, used by Smart Freestyle Changer
	# VERSE is FREESTYLE if it is ether complety Freestyle, or has more than 3 Freestylelines
	def _checkVerseforFreestyle(self, PLAYER):
		# Set Variables
		FREESTYLE = []
		FREE = True
		FREEBEATS = 0
		BEAT = 0
		NEWLINE = 0
		# Choose to correct Player
		if type(PLAYER) is int and 0 <= PLAYER <= 3:
			playerdict = eval("self.p" + str(PLAYER) + "song")
		else:
			raise ValueError("Player needs to be an integer between 0 and 3")
		try:  # Get Lastbeat
			lastbeat = max(playerdict[0].keys())
		except ValueError as Error:
			return (False, 'P' + str(PLAYER) + ' has no Lines')
		# Return False if strange shit happens
		while BEAT <= lastbeat:  # go trough song
			if BEAT in playerdict[1].keys() or BEAT == lastbeat:
				if FREE or FREEBEATS > 3:  # If we have a linebreak, write ether Verse as Freestyle
					FREESTYLE.append((NEWLINE, BEAT))
					NEWLINE = BEAT  # Reset Line
					FREEBEATS = 0
					FREE = True
				else:  # Or ignore it if it is not an Freestyleverse
					NEWLINE = BEAT  # Reset Line
					FREEBEATS = 0
					FREE = True
			if BEAT in playerdict[0].keys():
				if playerdict[0][BEAT][0] == ':':  # If we have normaly sung Lines, its not an freestyleverse
					FREE = False
				elif playerdict[0][BEAT][0] == 'F':
					FREEBEATS = FREEBEATS + 1
			BEAT = BEAT + 1
		return (True, FREESTYLE)  # Return succses and List of Tupels which lines are complete Freestyle

	# Moves Lines from PLAYER between START and END to the Front and Back by a certain amount of BEATS(+ or -)
	# COPY, should files be copyed or moved? REPLACE, if there are timing errors, should the old lines at that place be replaced?
	def moveLines(self, PLAYER, START, END, BEATS, COPY=False, REPLACE=False):
		if type(PLAYER) is int and 0 <= PLAYER <= 3:
			playerdict = eval("self.p" + str(PLAYER) + "song")
		else:
			raise ValueError("Player needs to be an integer between 0 and 3")
		# Should Lines that start before the End beat but end after it be copied?
		lines = sorted([x for x in playerdict[0].keys() if START <= x <= END])
		breaks = sorted([x for x in playerdict[1].keys() if START <= x <= END])
		linesToMove = dict()
		breaksToMove = dict()
		if lines[0] + BEATS < 0 or breaks[0] + BEATS < 0:
			raise ValueError(
				"Negative timmings are not allowed. To move the lines further fowrward, decrease the gap before the first line")
		if COPY:
			for key in lines:
				linesToMove[key + BEATS] = playerdict[0][key]
			for key in breaks:
				breaksToMove[key + BEATS] = playerdict[1][key]
		else:
			for key in lines:
				linesToMove[key + BEATS] = playerdict[0].pop(key)
			for key in breaks:
				breaksToMove[key + BEATS] = playerdict[1].pop(key)

		beats = set()
		for key in playerdict[0].keys():
			for x in range(playerdict[0][key][1]):
				if not x + key in beats:
					beats.add(x + key)
		beatsToMove = set()
		for key in linesToMove.keys():
			for x in range(linesToMove[key][1]):
				beatsToMove.add(x + key)
		if beats.isdisjoint(beatsToMove):
			for key, line in linesToMove.items():
				self.writenote(PLAYER, line[0], key, line[1], line[2], line[3])
			for key, line in breaksToMove.items():
				self.writebreak(PLAYER, key)
		elif REPLACE:
			collidingLines = [x for x in playerdict[0].keys if min(linesToMove.keys()) <= x <= max(linesToMove.keys()) +
							  linesToMove[max(linesToMove.keys())][1]]
			previousLines = sorted([x for x in playerdict[0].keys() if x < min(linesToMove.keys())])
			if previousLines:
				if previousLines[-1] + playerdict[0][previousLines[-1]][1] > min(linesToMove.keys()):
					collidingLines.append(previousLines[-1])
					collidingLines = sorted(collidingLines)
			print("The following Lines have been replaced")
			for key in collidingLines:
				print(playerdict[0].pop(key))
		else:
			# Failed, raise exception? return false?
			if not COPY:
				for key in linesToMove:
					playerdict[0][key - BEATS] = linesToMove[key]
				for key in breaksToMove:
					playerdict[1][key - BEATS] = breaksToMove[key]
		self.fixlinebreaks()

	# SENDS ALL LINES BETWEEN START and END form SPLAYER to DPLAYER to DSTART as Startingpoint
	def changeLinesPlayer(self, SSTART, SEND, SPLAYER, DPLAYER, DSTART, COPY=False, REPLACE=False):
		if type(SPLAYER) is int and 0 <= SPLAYER <= 3 and type(DPLAYER) is int and 0 <= DPLAYER <= 3:
			splayerdict = eval("self.p" + str(SPLAYER) + "song")
			dplayerdict = eval("self.p" + str(DPLAYER) + "song")
		else:
			raise ValueError("Player needs to be an integer between 0 and 3")
		# Should Lines that start before the End beat but end after it be copied?
		lines = sorted([x for x in splayerdict[0].keys() if SSTART <= x <= SEND])
		breaks = sorted([x for x in splayerdict[1].keys() if SSTART <= x <= SEND])
		linesToMove = dict()
		breaksToMove = dict()
		BEATS = DSTART - SSTART
		if DSTART < 0:
			raise ValueError(
				"Negative timmings are not allowed. To move the lines further fowrward, decrease the gap before the first line")
		if COPY:
			for key in lines:
				linesToMove[key + BEATS] = splayerdict[0][key]
			for key in breaks:
				breaksToMove[key + BEATS] = splayerdict[1][key]
		else:
			for key in lines:
				linesToMove[key + BEATS] = splayerdict[0].pop(key)
			for key in breaks:
				breaksToMove[key + BEATS] = splayerdict[1].pop(key)

		beats = set()
		for key in dplayerdict[0].keys():
			for x in range(dplayerdict[0][key][1]):
				if not x + key in beats:
					beats.add(x + key)
		beatsToMove = set()
		for key in linesToMove.keys():
			for x in range(linesToMove[key][1]):
				beatsToMove.add(x + key)
		if beats.isdisjoint(beatsToMove):
			for key, line in linesToMove.items():
				self.writenote(DPLAYER, line[0], key, line[1], line[2], line[3])
			for key, line in breaksToMove.items():
				self.writebreak(DPLAYER, key)
		elif REPLACE:
			collidingLines = [x for x in dplayerdict[0].keys if
							  min(linesToMove.keys()) <= x <= max(linesToMove.keys()) +
							  linesToMove[max(linesToMove.keys())][1]]
			previousLines = sorted([x for x in dplayerdict[0].keys() if x < min(linesToMove.keys())])
			if previousLines:
				if previousLines[-1] + dplayerdict[0][previousLines[-1]][1] > min(linesToMove.keys()):
					collidingLines.append(previousLines[-1])
					collidingLines = sorted(collidingLines)
			print("The following Lines have been replaced")
			for key in collidingLines:
				print(dplayerdict[0].pop(key))
		else:
			# Failed, raise exception? return false?
			if not COPY:
				for key in linesToMove:
					splayerdict[0][key - BEATS] = linesToMove[key]
				for key in breaksToMove:
					splayerdict[1][key - BEATS] = breaksToMove[key]
		self.fixlinebreaks()

	# Copies from a different Songobject all lines from SPLAYER between SSTART and SEND to DPLAYER,DSTART
	def copyfromsong(self, OTHERSONG, SPLAYER, SSTART, SEND, DPLAYER, DSTART, REPLACE=False):
		if type(SPLAYER) is int and 0 <= SPLAYER <= 3 and type(DPLAYER) is int and 0 <= DPLAYER <= 3:
			splayerdict = eval("self.p" + str(SPLAYER) + "song")
			dplayerdict = eval("self.p" + str(DPLAYER) + "song")
		else:
			raise ValueError("Player needs to be an integer between 0 and 3")
		# Should Lines that start before the End beat but end after it be copied?
		lines = sorted([x for x in splayerdict[0].keys() if SSTART <= x <= SEND])
		breaks = sorted([x for x in splayerdict[1].keys() if SSTART <= x <= SEND])
		linesToMove = dict()
		breaksToMove = dict()
		BEATS = DSTART - SSTART
		if DSTART < 0:
			raise ValueError(
				"Negative timmings are not allowed. To move the lines further fowrward, decrease the gap before the first line")
		for key in lines:
			linesToMove[key + BEATS] = splayerdict[0][key]
		for key in breaks:
			breaksToMove[key + BEATS] = splayerdict[1][key]

		beats = set()
		for key in dplayerdict[0].keys():
			for x in range(dplayerdict[0][key][1]):
				if not x + key in beats:
					beats.add(x + key)
		beatsToMove = set()
		for key in linesToMove.keys():
			for x in range(linesToMove[key][1]):
				beatsToMove.add(x + key)
		if beats.isdisjoint(beatsToMove):
			for key, line in linesToMove.items():
				self.writenote(DPLAYER, line[0], key, line[1], line[2], line[3])
			for key, line in breaksToMove.items():
				self.writebreak(DPLAYER, key)
		elif REPLACE:
			collidingLines = [x for x in dplayerdict[0].keys if
							  min(linesToMove.keys()) <= x <= max(linesToMove.keys()) +
							  linesToMove[max(linesToMove.keys())][1]]
			previousLines = sorted([x for x in dplayerdict[0].keys() if x < min(linesToMove.keys())])
			if previousLines:
				if previousLines[-1] + dplayerdict[0][previousLines[-1]][1] > min(linesToMove.keys()):
					collidingLines.append(previousLines[-1])
					collidingLines = sorted(collidingLines)
			print("The following Lines have been replaced")
			for key in collidingLines:
				print(dplayerdict[0].pop(key))
		else:
			# Failed, raise exception? return false?
			pass
		self.fixlinebreaks()

	# --------------------------- #
	# Start Object self Diagnosis #
	# --------------------------- #

	# Check if a file is relative
	def checkRelative(self,lyrics):
		if re.search("#RELATIVE:YES", lyrics, re.IGNORECASE):
			self.tags['RELATIVE'] = "YES"
			return True
		lines = lyrics.splitlines()
		for i in range(1, len(lines)):
			if re.match("- ?\d+(?: \d+)?", lines[i - 1]):
				# Next line doesn't start at 0 and is not empty, the End or a player change
				if not (lines[i][1:4] == " 0 " or re.match("(?:P ?\d)?E?\s*$", lines[i])):
					return False
		self.tags['RELATIVE'] = "YES"
		return True


	# Scanning for strange Player/Duett behavior
	def duettscan(self):
		# Set to True as "Standart"
		# Because if there are Errors here, then there are more Players in some strange way.
		# And therefor it is an Duett(kind of)
		self.duett = True
		# Mesuere Number of Things each Player has to sing.
		P0notes = len(self.p0song[0])
		P0breaks = len(self.p0song[1])
		P1notes = len(self.p1song[0])
		P1breaks = len(self.p1song[1])
		P2notes = len(self.p2song[0])
		P2breaks = len(self.p2song[1])
		P3notes = len(self.p3song[0])
		P3breaks = len(self.p3song[1])
		# Match Patterns to find out what kind of SOng it is:
		# If song is sung by only P0, e.g. normal
		if (P0notes > 0):
			# Check if there are no other lines sung by someone else(there shouldnt be any)
			if (P1notes == 0 and P2notes == 0 and P3notes == 0 and P1breaks == 0 and P2breaks == 0 and P3breaks == 0):
				# Then its a normals Song, e.g. no Duett and no Error
				self.duett = False
				self.vitals['DuettPlayerError'] = False
			else:  # Write Error
				self.vitals['DuettPlayerError'] = True
		# If P0 sings nothing song is Probably a Duett:
		# Check if P1 and P2 sing?
		elif (P1notes > 0):
			if (P2notes > 0):
				# Check if nothing realy REALY Strange happend?
				if (P0breaks == 0):
					# If not, then self.duett=True is already set, so delete the DuettPlayerError!
					self.vitals['DuettPlayerError'] = False
				else:  # Write Errors
					self.vitals['DuettPlayerError'] = True
			else:
				self.vitals['DuettPlayerError'] = True
		else:
			self.vitals['DuettPlayerError'] = True

	# Prints out any Errors, that eather occured
	# Also returns Triple of SYNTAX: (Exist Repaired Errors?, Exist Problems?, Exist Vital Problems?)
	# You can pass False to hide certain kind of Errors.
	# for example: song.echoerrors(False,True,False) shows only Errors of the "Problem Categorie"
	def echoerrors(self, showR=True, showP=True, showO=True, showV=True, ):
		# Refresh Duettstatus
		self.duettscan()
		self.timingscan()
		# Find out if there are any open Errors?
		P = self.problems['UnusualLine'][0] or self.problems['StrangeLinebreaks'][0]
		V = self.vitals['DTN'][0] or self.vitals['DuettPlayerError']
		R = self.repaired['MissEnd'] or self.repaired['WrongEnds'] or self.repaired['UnknownTag'][0] or \
			self.repaired['StrangeLyric'][0]
		O = self.others['SimNoteP0'][0] or self.others['SimNoteP1'][0] or self.others['SimNoteP2'][0] or \
			self.others['SimNoteP3'][0]
		# Print them:
		if R and showR:
			print(
			'There are Things wrong with the Source, they will be corrected as good we can, please check usdxsong.repaired for more Information')
		if P and showP:
			print('There are Problems with this File, please check usdxsong.problems for more Information')
		if O and showO:
			print('There are other Problems with this File, please check usdxsong.others fro more Information')
		if V and showV:
			print('There are Vital Problems with this File, please check usdxsong.vitals for more Information')
		return (R, P, O, V)

	# Scans the File for Timingerrors, e.g. Notes that need to be sung simultaniously
	def timingscan(self):
		beats = set()
		# Add all beats where P0 is currently singing to notes
		for key in self.p0song[0].keys():
			for x in range(self.p0song[0][key][1]):
				if not x + key in beats:
					beats.add(x + key)
				else:
					self.others["SimNoteP0"][0] = True
					self.others["SimNoteP0"][1].append(x + key)

		beats = set()
		# Add all beats where P1 is currently singing to notes
		for key in self.p1song[0].keys():
			for x in range(self.p1song[0][key][1]):
				if not x + key in beats:
					beats.add(x + key)
				else:
					self.others["SimNoteP1"][0] = True
					self.others["SimNoteP1"][1].append(x + key)

		beats = set()
		# Add all beats where P2 is currently singing to notes
		for key in self.p2song[0].keys():
			for x in range(self.p2song[0][key][1]):
				if not x + key in beats:
					beats.add(x + key)
				else:
					self.others["SimNoteP2"][0] = True
					self.others["SimNoteP2"][1].append(x + key)

		beats = set()
		# Add all beats where P3 is currently singing to notes
		for key in self.p3song[0].keys():
			for x in range(self.p3song[0][key][1]):
				if not x + key in beats:
					beats.add(x + key)
				else:
					self.others["SimNoteP3"][0] = True
					self.others["SimNoteP3"][1].append(x + key)

	# ---------------------------------------- #
	# View Song on Screen, or Print it to File #
	# ---------------------------------------- #

	# Print itself on Screen. Prints what would also be Written:
	def view(self):
		# Refresh Duettstatus
		self.duettscan()
		self.timingscan()
		print('Previev of: ' + self.tags['TITLE'] + ' from ' + self.tags['ARTIST'] + ':')
		print(' ')
		self.echoerrors()
		print(' ')
		print('Start Previev:')
		print('========================')
		# Start printing Song:
		# Print existing TAGs
		for T in self.tags:
			if self.tags[T] != '':
				print('#' + T + ':' + self.tags[T])
		for player in range(4):
			playerdict = eval("self.p" + str(player) + "song")

			try:
				lastbeat = max(max(playerdict[0].keys()), max(playerdict[1].keys()))
			except ValueError as Error:
				lastbeat = 0
			# We only need to write Player if he has something to sing!
			if lastbeat > 0:
				if player > 0:
					print('P' + str(player))
				lastbeat = max(max(playerdict[0].keys()), max(playerdict[1].keys()))
				BEAT = 0
				# For every Beat, from first to last, print what will be sung at that beat
				while (BEAT <= lastbeat):
					# Print Breaks(Single and Double)
					# Of course print only when there is something to sing in the first place
					if BEAT in playerdict[1]:
						if (len(playerdict[1][BEAT]) == 1):
							print('- ' + str(BEAT))
						else:
							print('- ' + str(BEAT) + str(playerdict[1][BEAT][1]))
					# Print Notes
					# Of course print only when there is something to sing in the first place
					if BEAT in playerdict[0]:
						print(playerdict[0][BEAT][0] + ' ' + str(BEAT) + ' ' + str(playerdict[0][BEAT][1]) + ' ' + str(
								playerdict[0][BEAT][2]) + ' ' + playerdict[0][BEAT][3])
					BEAT = BEAT + 1
		print('E')  # Ending E

	# Write itself to File
	# Inputs are:
	# Name(full valid path) of new File. Else old File will be Replaced if allowed.(String)
	# Should Problems be ignored?(Booleen)
	# Should you replace Files("replaced" Files are renamed to: OLDFILENAME.bkup)(Booleen)
	# To delete all old .bkup files use usdxutils
	# Output is the Written File
	# Returns Tupel: (BOOLEEN,'STRING')
	# ([Was the Writing succssesful? False if Not.],[Additional Infos])
	def writeToFile(self, FULL='', replace=True, ignoreProblems=False, PrintToScreen=False):
		# Refresh Duetstatus
		self.duettscan()
		self.timingscan()
		# Get Name and Directory
		if FULL == '':
			FULL = self.path
			PWD, NAME = os.path.split(self.path)
			if not os.path.isdir(PWD):
				return (False, 'PathError')
			if os.path.isfile(FULL) and (not replace):
				return (False, 'Overwriting Files not Active')
		elif len(FULL.split(os.path.sep)) > 1:
			PWD, NAME = os.path.split(FULL)
			if not os.path.isdir(PWD):
				return (False, 'Invalid Path')
			if os.path.isfile(FULL) and (not replace):
				return (False, 'Overwriting Files not Active')
			if NAME.rsplit('.', 1)[1] != 'txt':
				return (False, 'Plaintextfiles should end on .txt')
		else:
			return (False, 'Please give a valid Path: "[PATHTOFILE]/[FILE].txt"')

		# Get Errors
		print('Remaining Errors:')
		(Rep, Pro, Oth, Vit) = self.echoerrors()
		print(' ')
		if Vit:
			print('There are Vital errors, writing Aborted')
			print('Please correct before Writing')
			return (False, 'Vital Errors')
		elif (Pro and (not ignoreProblems)):
			print('There are Problems with some Lines in this Song')
			print('Writing Aborted! If you want to write Anyway set ignoreProblems to True')
			return (False, 'Remaining Line Problems')
		elif (Oth and (not ignoreProblems)):
			print('There are Problems with some Timing in this Song')
			print('Writing Aborted! If you want to write Anyway set ignoreProblems to True')
			return (False, 'Remaining Other Problems')

		# All obvious failing cases handled
		# So We Write
		else:
			# Warn that Problems being ignored
			if (Oth or Pro):
				print('There are Problems, they will be ignored at Writing!')

			# Creating Backup if Necessary:
			if os.path.isfile(FULL) and replace:
				BACKUP = FULL.rsplit('.', 1)[0] + '.bkup'
				if os.path.isfile(BACKUP):
					# If old Backup already exist make one fresh backup, which will be the newest(and owerwritten every time you use self.write())
					BACKUP = FULL.rsplit('.', 1)[0] + '(newest).bkup'
					os.replace(FULL, BACKUP)
				else:
					# If no Backup exist until now create first backup, which will stay no matter how often you use self.write()
					os.replace(FULL, BACKUP)
				time.sleep(0.2)

			if PrintToScreen:
				self.view()
				print('========================')
				print('End of Preview')
			# Start writing Song:
			# Basically same Code as in usdxsong.view()
			# Just writing into a file, instead of printing to screen
			# Additionally printing \n behind every line
			try:
				with open(FULL, 'w', encoding="UTF-8") as FILE:
					# Print existing TAGs
					for T in self.tags:
						if self.tags[T] != '':
							FILE.write('#' + T + ':' + self.tags[T] + '\n')
					for player in range(4):
						playerdict = eval("self.p" + str(player) + "song")

						try:
							lastbeat = max(max(playerdict[0].keys()), max(playerdict[1].keys()))
						except ValueError as Error:
							lastbeat = 0
						# We only need to write Player if he has something to sing!
						if lastbeat > 0:
							if player > 0:
								FILE.write('P' + str(player) + '\n')
							lastbeat = max(max(playerdict[0].keys()), max(playerdict[1].keys()))
							BEAT = 0
							# For every Beat, from first to last, print what will be sung at that beat
							while (BEAT <= lastbeat):
								# Print Breaks(Single and Double)
								# Of course print only when there is something to sing in the first place
								if BEAT in playerdict[1]:
									if (len(playerdict[1][BEAT]) == 1):
										FILE.write('- ' + str(BEAT) + '\n')
									else:
										FILE.write('- ' + str(BEAT) + str(playerdict[1][BEAT][1]) + '\n')
								# Print Notes
								# Of course print only when there is something to sing in the first place
								if BEAT in playerdict[0]:
									FILE.write(playerdict[0][BEAT][0] + ' ' + str(BEAT) + ' ' + str(
											playerdict[0][BEAT][1]) + ' ' + str(playerdict[0][BEAT][2]) + ' ' +
											   playerdict[0][BEAT][3] + '\n')
								BEAT = BEAT + 1
					FILE.write('E')  # Ending E
					return (True, 'Success')
			# Finished writing
			# Errorhandling
			except Exception as ERROR:
				print(NAME + ": File throws strange Errors!, It wasnt written!")
				if os.path.isfile(FULL) and replace:
					try:
						os.remove(FULL)
						time.sleep(0.2)
					except Exception as Error:
						pass
					os.replace(BACKUP, FULL)
					time.sleep(0.2)
				return (False, 'Strange Exception: ' + ERROR)

	# ------------ #
	# Split Lines #
	# ------------ #

	# Split lines into Type, Time, Length, Pitch, Lyric
	def linesplitter(self, LINE, PLAYER):
		# Group 1: Linetype (: F or *)
		# Group 2: Beat
		# Group 3: Duration
		# Group 4: Pitch
		# Group 5: Text
		SPLIT = re.match("([:F*]) (\d+) (\d+) (-?\d+) (.+)", LINE)
		INFO = []
		# First Column is : F or * and there are 5 columns
		if (SPLIT):
			INFO.append(SPLIT.group(1))
			INFO.append(int(SPLIT.group(2)))
			INFO.append(int(SPLIT.group(3)))
			INFO.append(int(SPLIT.group(4)))
			INFO.append(SPLIT.group(5))
		else:
			# Add Problem
			self.problems["UnusualLine"][0] = True
			self.problems["UnusualLine"][1].append('P' + str(PLAYER) + ' ' + LINE)
			return (False, INFO)

		# Check whether Lyrics have no Whitespace inside theirself
		if len(str(SPLIT.group(5)).split()) != 1:
			# Add Problem
			self.repaired['StrangeLyric'][0] = True
			self.repaired['StrangeLyric'][1].append('P' + str(PLAYER) + ' ' + LINE)

		return (True, INFO)

	# -------------------------------- #
	# Write Notes or Breaks to Players #
	# -------------------------------- #

	# Write all Notes sung by Player Three to Player One and Two unless it collides with other Notes
	def splitP3(self):
		beats = set()
		# Add all beats where P1 is currently singing to notes
		for key, line in self.p1song[0].items():
			for x in range(line[1]):
				beats.add(x + key)
		# Add all notes where P2 is currently singing to notes
		for key in self.p2song[0].keys():
			for x in range(self.p2song[0][key][1]):
				beats.add(x + key)
		removeLines = []
		for time in self.p3song[0].keys():
			note = self.p3song[0][time]
			if beats.isdisjoint(set([time + x for x in range(note[1])])):
				self.writenote(1, note[0], time, note[1], note[2], note[3])
				self.writenote(2, note[0], time, note[1], note[2], note[3])
				removeLines.append(time)
			else:
				self.others["SimNoteP3"][0] = True
		for time in removeLines:
			self.p3song[0].pop(time)
		for key, line in self.p3song[1].items():
			self.writebreak(1, key)
			self.writebreak(2, key)
		self.fixlinebreaks()

	# Fix length of doublelinebreaks and remove duplicate linebreaks
	def fixlinebreaks(self):
		for player in range(3):
			playerdict = eval("self.p" + str(player) + "song")

			removeLinebreaks = []
			for key, line in playerdict[1].items():
				# if any line follows the linebreak
				if [x for x in playerdict[0].keys() if x >= key]:
					# find closest following line
					nextLine = sorted([x for x in playerdict[0].keys() if x >= key])[0]
				else:
					# if there is no following line, remove the linebreak
					playerdict[1][key].pop()
					continue
				# Check for linebreaks between this linebreak and the beginning of the next line with lyrics
				intermediateLinebreaks = [x for x in playerdict[1].keys() if key < x < nextLine]
				# remove all surplus linebreaks
				for linebreak in intermediateLinebreaks:
					removeLinebreaks.append(linebreak)
				# In case of double linebreaks check spacing
				if len(line) == 2 and key + line[1] > nextLine:
					# remove second number and let the game decide when to put up the lyric
					playerdict[1][key].pop()
				for linebreak in removeLinebreaks:
					playerdict[1].pop(linebreak)

	# Write Notes(Normal, Golden or Freestyle) to the Player who sings it
	# Use Startbeat(TIME) as DictonaryKEY
	# If there are 2 Notes sung by the same Player at the same Time the secong Thing will be added to the DTNs in self.vital
	# SYNTAX: PLAYERSDICTONARY: TIME: [TYPE,LENGTH,PITCH,LYRIC]
	def writenote(self, PLAYER, TYPE, TIME, LENGTH, PITCH, LYRIC):
		# Write to correct Player
		if type(PLAYER) is int and 0 <= PLAYER <= 3:
			playerdict = eval("self.p" + str(PLAYER) + "song")
		else:
			raise ValueError("Player needs to be an integer between 0 and 3")
		if TIME in playerdict[0]:
			#print(TIME, playerdict[0][TIME])
			self.addDTN(TYPE + ' ' + str(TIME) + ' ' + str(LENGTH) + ' ' + str(PITCH) + ' ' + LYRIC, PLAYER)
		else:
			playerdict[0][TIME] = [TYPE, LENGTH, PITCH, LYRIC]

	# End of writenote()

	# Write Breaks to the Player who "sings" them
	# Use Startbeat(TIME) as DictonaryKEY
	# If there are 2 Breaks "sung" by the same Player at the same Time the second Thing will be added to the DTNs in self.vital
	# SYNTAX: PLAYERSDICTONARY: TIME: TYPE[, BREAKEND]
	def writebreak(self, PLAYER, TIME):
		# Write to correct Player
		if type(PLAYER) is int and 0 <= PLAYER <= 3:
			playerdict = eval("self.p" + str(PLAYER) + "song")
		else:
			raise ValueError("Player needs to be an integer between 0 and 3")
		if TIME in playerdict[1]:
			self.addDTN('- ' + str(TIME), PLAYER)
		else:
			playerdict[1][TIME] = ['-']

	# End of writebreak()

	# ---------------------------------------------- #
	# Add and Delete Things in the Errordictionarys: #
	# ---------------------------------------------- #

	# Add DoubleTimeError to self.vitals
	def addDTN(self, LINE, PLAYER):
		# Set DTN-Existence to True
		self.vitals['DTN'][0] = True
		# Add Problem
		self.vitals['DTN'][1].append('P' + str(PLAYER) + ' ' + LINE)

	# Delete DoubleTimeError
	# Does nothing ithis specific DTN does not exist
	def delDTN(self, LINE):
		# Try Deleting Problem
		if self.vitals['DTN'][0] == True:
			try:
				self.vitals['DTN'][1].remove(LINE)
				# If this was the last DTN, set DTN-Existence back to False
				if self.vitals['DTN'][1] == []:
					self.vitals['DTN'][0] = False
			except ValueError as Error:
				pass

	# Add Problem to self.problem
	def addproblem(self, TYPE, LINE, PLAYER):
		# Set Problemtype to True
		self.problems[TYPE][0] = True
		# Add Problem
		self.problems[TYPE][1].append('P' + str(PLAYER) + ' ' + LINE)

	# Delete Problem
	# Does nothing if this specific Problem does not exist
	def delproblem(self, TYPE, LINE):
		# Try Deleting Problem
		if self.problems[TYPE][0] == True:
			try:
				self.problems[TYPE][1].remove(LINE)
				# If this was the last Problem, set Problemtype back to False
				if self.problems[TYPE][1] == []:
					self.problems[TYPE][0] = False
			except ValueError as Error:
				pass

	# Empty Lines are stupid problems that dont result in Problems and can be removed easily
	def removeemptylines(self):
		for P in self.problems['UnusualLine'][1]:
			if P == 'P0 \n':
				self.delproblem('UnusualLine', 'P0 \n')
			if P == 'P0 \r\n':
				self.delproblem('UnusualLine', 'P0 \r\n')
			if P == 'P1 \n':
				self.delproblem('UnusualLine', 'P1 \n')
			if P == 'P1 \r\n':
				self.delproblem('UnusualLine', 'P1 \r\n')
			if P == 'P2 \n':
				self.delproblem('UnusualLine', 'P2 \n')
			if P == 'P2 \r\n':
				self.delproblem('UnusualLine', 'P2 \r\n')

# --------------------- #
# End of usdxsong class #
# --------------------- #
